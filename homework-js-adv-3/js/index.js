"use strict";

// task1

// const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
//
// const newarray = new Set([...clients1, ...clients2]);
// console.log(newarray)


// task2

// const characters = [
//     {
//         name: "Елена",
//         lastName: "Гилберт",
//         age: 17,
//         gender: "woman",
//         status: "human"
//     },
//     {
//         name: "Кэролайн",
//         lastName: "Форбс",
//         age: 17,
//         gender: "woman",
//         status: "human"
//     },
//     {
//         name: "Аларик",
//         lastName: "Зальцман",
//         age: 31,
//         gender: "man",
//         status: "human"
//     },
//     {
//         name: "Дэймон",
//         lastName: "Сальваторе",
//         age: 156,
//         gender: "man",
//         status: "vampire"
//     },
//     {
//         name: "Ребекка",
//         lastName: "Майклсон",
//         age: 1089,
//         gender: "woman",
//         status: "vempire"
//     },
//     {
//         name: "Клаус",
//         lastName: "Майклсон",
//         age: 1093,
//         gender: "man",
//         status: "vampire"
//     }
// ];
//
// const [{name, lastName, age}] = characters;
// const charactersShortInfo = [];
// for (let i = 0; i < characters.length; i++) {
//     const {name, lastName, age} = characters[i];
//     charactersShortInfo.push({name, lastName, age});
// }
// console.log(charactersShortInfo)


// task3

// const user1 = {
//     name: "John",
//     years: 30
// }
//
// let {name, years:age, isAdmin = false} = user1;
//
// console.log(name);
// console.log(age);
// console.log(isAdmin);


// task4
//
// const satoshi2020 = {
//     name: 'Nick',
//     surname: 'Sabo',
//     age: 51,
//     country: 'Japan',
//     birth: '1979-08-21',
//     location: {
//         lat: 38.869422,
//         lng: 139.876632
//     }
// }
//
// const satoshi2019 = {
//     name: 'Dorian',
//     surname: 'Nakamoto',
//     age: 44,
//     hidden: true,
//     country: 'USA',
//     wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//     browser: 'Chrome'
// }
//
// const satoshi2018 = {
//     name: 'Satoshi',
//     surname: 'Nakamoto',
//     technology: 'Bitcoin',
//     country: 'Japan',
//     browser: 'Tor',
//     birth: '1975-04-05'
// }
// const s2018 = new Map(Object.entries(satoshi2018));
// const s2019 = new Map(Object.entries(satoshi2019));
// const s2020 = new Map(Object.entries(satoshi2020));
//
// const merged = new Map([...s2018, ...s2019, ...s2020])
// const obj = Object.fromEntries(merged);
// console.log(obj);


// task5

// const books = [{
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
// }, {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
// }, {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
// }];
//
// const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
// }
//
// const newArray = [...books,bookToAdd];
// console.log(newArray);


// task6
//
// const employee = {
//     name: 'Vitalii',
//     surname: 'Klichko'
// }
// const {name, surname, age = 18, salary = 0} = employee;
// const newObj = {name, surname, age, salary}
// console.log(newObj);


// task7

// const array = ['value', () => 'showValue'];
// const [value, showValue] = array;
//
// alert(value);
// alert(showValue());

