"use strict";

const tabListActions = document.querySelector('.tabs-header');
const tabListContent = document.querySelector('.tabs-content-list');
const filterListActions = document.querySelector('.filter-header');
const filterListContent = document.querySelector('.figure-wrapper');
const listOfIds = ['first-img', 'second-img', 'third-img', 'fourth-img', 'five-img']

tabListActions.addEventListener("click", choseTabText);
filterListActions.addEventListener("click", filterImgs);

onInit();

function choseTabText(event) {
    const target = event.target.closest('li');
    const eventTargetId = target.getAttribute('id');
    changeActiveTab(eventTargetId);
    tabFilter(eventTargetId);
}

function filterImgs(event) {
    const target = event.target.closest('li');
    const eventTargetId = target.getAttribute('id');
    changeActiveFilter(eventTargetId);
    filterImgById(eventTargetId);
}


function changeActiveTab(eventTargetIdSelected) {
    const listOfTabsElements = [...tabListActions.getElementsByTagName('li')]
    listOfTabsElements.forEach(el => {
        if (!(el.getAttribute('id').startsWith(eventTargetIdSelected))) {
            el.classList.remove('active');
        } else {
            el.classList.add('active');
        }
    });
}

function changeActiveFilter(eventTargetIdSelected) {
    const listOfTabsElements = [...filterListActions.getElementsByTagName('li')]
    listOfTabsElements.forEach(el => {
        const span = el.getElementsByTagName('span');
        if (!(el.getAttribute('id').startsWith(eventTargetIdSelected))) {
            el.classList.remove('active');
            span.item(0).classList.remove('active');
        } else {
            span.item(0).classList.add('active');
            el.classList.add('active');
        }
    });
}

function onInit() {
    tabFilter(getInitialActiveTabId());
}

function tabFilter(idInit) {
    const listOfElements = [...tabListContent.getElementsByTagName('li')]
    listOfElements.forEach(el => {
        if (!(el.getAttribute('id').startsWith(idInit))) {
            el.className = 'hide-element'
        } else {
            el.className = 'show-element'
        }
    });
}

function getInitialActiveTabId() {
    const listOfElements = [...tabListActions.getElementsByTagName('li')]
    return listOfElements.find(el => el.classList.contains('active')).id;
}

function getInitialActiveTabIdFilter() {
    const listOfElements = [...filterListActions.getElementsByTagName('li')]
    return listOfElements.find(el => el.classList.contains('active')).id;
}


const loadBtn = document.getElementById('load-img-btn');
loadBtn.addEventListener('click', ladImges);


function ladImges() {
    loadBtn.className = 'hide-element';
    createImges();
    filterImgById(getInitialActiveTabIdFilter());
    loadBtn.removeEventListener('click', ladImges);
}


function createImges() {
    const wrapper = document.getElementById('filter-figure-wrapper');
    const element = wrapper.getElementsByClassName('figure-item').item(0);
    cloneAndAddImg(element, wrapper);
}


function cloneAndAddImg(element, wrapper) {
    for (let i = 1; i <= 12; i++) {
        const elementCloned = element.cloneNode(true);
        elementCloned.classList.add('nature-design');
        elementCloned.classList.remove('web-design');
        let imgNode = elementCloned.childNodes.item(1).childNodes.item(1).childNodes.item(1);
        imgNode.src = `./assets/img/amazing-work/new/img${i}.png`;
        wrapper.append(elementCloned);
    }
}

function filterImgById(eventTargetId) {
    const listOfElements = [...filterListContent.getElementsByTagName('figure')]
    if (eventTargetId !== 'all') {
        listOfElements.forEach(el => {
            if (!(el.classList.contains(eventTargetId))) {
                el.classList.add('hide-element');
            } else {
                el.classList.remove('hide-element');
            }
        });
    } else {
        listOfElements.forEach(el => {
            el.classList.remove('hide-element');
        });
    }
}

const listOfimges = [...document.getElementsByClassName('carousel-img')];

$(document).ready(function () {
    (function ($) {
        $('.owl-carousel').owlCarousel({
            items: 4,
            margin: 38,
            loop: false,
            dots: false
        });
    })(jQuery);
    const buttonLeft = document.getElementById('carusel-arrows-left')
    const buttonRight = document.getElementById('carusel-arrows-right')
    buttonLeft.addEventListener("click", stepLeft);
    buttonRight.addEventListener("click", stepRight);
    let owl = $('.owl-carousel');
    owl.owlCarousel();
    owl.on('click', function (event) {
        if (listOfIds.includes(event.target.id)) {
            listOfimges.forEach(el => {
                el.classList.remove('move')
            })
            event.target.classList.add('move');
            choseReviews(event.target.id)
        }
    });
});


function choseReviews(id) {
    const listOfElements = [...document.getElementsByClassName('selected-review-container')];
    listOfElements.forEach(el => {
        if (el.classList.contains(id)) {
            el.classList.remove('hide-element');
        } else {
            el.classList.add('hide-element');
        }
    });
}

function stepLeft() {
    $('.owl-carousel').trigger('prev.owl.carousel')
}

function stepRight() {
    $('.owl-carousel').trigger('next.owl.carousel')
}















