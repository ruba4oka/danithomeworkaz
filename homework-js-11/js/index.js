"use strict";

const root = document.getElementById('all-btns');
document.addEventListener("keydown", getButtonsClicked)


function getButtonsClicked(event) {
    const eventKey = event.code === 'Enter' ? 'Enter' : event.code[event.code.length - 1].toUpperCase();
    choseBtn(root, eventKey);
}

function choseBtn(root, eventKey) {
    const array = [...root.children];
    let element;
    array.forEach(el => {
        if (el.innerText === eventKey) {
            element = el;
        }
    });
    if (element !== undefined) {
        array.forEach(el => {
            el.classList.remove('color-blue');
        });
        element.classList.add('color-blue');
    }
}

