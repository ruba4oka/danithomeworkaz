"use strict";

const root = document.getElementById('root');
const btn = document.getElementById('get-ip');
btn.addEventListener("click", getIp);

async function getIp() {
    try {
        const response = await fetch('https://api.ipify.org/?format=json');
        const parsedInfo = await response.json();
        const {ip} = parsedInfo;
        await getAllData(ip);
    } catch (error) {
        console.log(error);
    }
}

async function getAllData(ip) {
    const constAllDataResponse = await fetch(`http://ip-api.com/json/${ip}?fields=continent,country,regionName,city,district,query`);
    const parsedInfo = await constAllDataResponse.json();
    const infoToPublic = new IpInfo(parsedInfo)
    infoToPublic.renderIpInfo();
}

class IpInfo {
    constructor({continent = '', country = '', regionName = '', city = '', district = '', root}) {
        this.continent = continent;
        this.country = country;
        this.regionName = regionName;
        this.city = city;
        this.district = district;
        this.root = root;
    }

    renderIpInfo() {
        const ul = document.createElement("ul");
        this.createLi('Континент : ', this.continent, ul);
        this.createLi('Країна : ', this.country, ul);
        this.createLi('Регіон : ', this.regionName, ul);
        this.createLi('Місто : ', this.city, ul);
        this.createLi('Район : ', this.district, ul);
        root.append(ul);
    }

    createLi(name, value, ul) {
        const li = document.createElement("li");
        li.textContent = name + ' ' + value;
        ul.append(li);
    }
}
