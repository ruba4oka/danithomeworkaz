let firstNumber;
let secondNumber;
let action;
let firstAttempt = false;
do {
    firstNumber = prompt('Enter first number', firstAttempt ? 1 : undefined);
    secondNumber = prompt('Enter second number', firstAttempt ? 2 : undefined);
    action = prompt('Enter action', firstAttempt ? '+' : undefined);
    firstAttempt = true;
} while (validateNumber(firstNumber) || validateNumber(secondNumber) || !validateAction(action));

console.log(doMathActions(firstNumber, secondNumber, action));

function validateNumber(numbReceived) {
    return isNaN(+numbReceived) || numbReceived === "" || numbReceived === null
}

function validateAction(action) {
    let answer = false;
    switch (action) {
        case '+':
        case '-':
        case '/':
        case '*':
            answer = true;
            break;
    }
    return answer;
}

function doMathActions(firstNumber, secondNumber, action) {
    let result;
    const firstNumberParsed = +firstNumber;
    const secondNumberParsed = +secondNumber;
    switch (action) {
        case '+':
            result = firstNumberParsed + secondNumberParsed;
            break;
        case '-':
            result = firstNumberParsed - secondNumberParsed;
            break;
        case '/':
            result = firstNumberParsed / secondNumberParsed;
            break;
        case '*':
            result = firstNumberParsed * secondNumberParsed;
            break;
    }
    return result;
}