"use strict";

// let num = 1;
// num++;
// console.log(num);
// num += 1;
// num = num + 1;

/*
numIt     start i      finish i
1.       0              2
2.       2              4
3.       4              6
*/
// const MAX_ITER = 333;
// const MIN_ITER = 15;

// label: for (let a = MAX_ITER; a > MIN_ITER; a--) {
//     //  i++;
//     if (a === 44) {
//         //   break label;
//         continue label;
//     }
//     console.log("Hello " + a);
// }

// console.log(i);
// for (let i = 5; i > 0; i--) {
//     //  i++;
//     console.log("Hello " + i);
// }

// const MAX_ITER = 333;
// const MIN_ITER = 15;
// let i = 0;

// while (i > MIN_ITER) {
//     i++;
//     console.log(i);
// }

// do {
//     var a = 3;
//     i++;
//     console.log(i);
// } while (false);

// var a = 3;
// if (true) {
//     var a = 5;
// }

// var a;
// console.log(window.a);
// console.log(window);

// window.alert("hi");


// -  Задание 2.
// -
// -  Пользователь должен ввести два числа.
// -  Если введённое значение не является числом,
//     -  программа продолжает опрашивать пользователя до тех пор,
//     -  пока он не введёт число.
// -
// -  Когда пользователь введёт два числа, вывести в консоль сообщение:
//     -  «Поздравляем. Введённые вами числа: «ПЕРВОЕ*ЧИСЛО» и «ВТОРОЕ*ЧИСЛО».». \*/
//


// let answerFirst;
// let answerSecond;
// do {
//     answerFirst = prompt('Enter first number');
// } while (isNaN(+answerFirst) || answerFirst==="" || answerFirst===null);
//
// do {
//     answerSecond = prompt('Enter second number');
// } while (isNaN(+answerSecond) || answerFirst==="" || answerSecond===null);
//
// alert( `Поздравляем. Введённые вами числа: ${answerFirst} и ${answerSecond}`);


//
// -  Программа должна узнать у пользователя его:
//     -  -  Имя;
// -  -  Фамилию;
// -  -  Год рождения.
// -
// -  Если пользователь вводит некорректные данные,
//     -  программа должна повторно опрашивать его до тех пор,
//     -  пока данные не будут введены корректно.
// -
// -  Данные считается введёнными некорректно, если:
// -  -  Пользователь не вводит в поле никаких данных;
// -  -  Год рождения, введённый пользователем меньше, чем 1910 или больше, чем
// текущий год.
// -
// -  Когда пользователь введёт все необходимые данные корректно,
//     -  вывести в консоль сообщение:
//     -  «Добро пожаловать, родившийся в ГОД_РОЖДЕНИЯ, ИМЯ ФАМИЛИЯ.». \*/


let answerFirst;
let answerSecond;
let answerThird;
do {
    answerFirst = prompt('Enter name');
} while (answerFirst==="" || answerFirst===null);

do {
    answerSecond = prompt('Enter second name');
} while (answerFirst==="" || answerSecond===null);

do {
    answerThird = prompt('Enter year of birthday');
} while (isNaN(+answerThird) || answerThird==="" || answerThird===null || +answerThird<1910 || +answerThird>2021);

alert( `Добро пожаловать, родившийся в ${answerThird}, ${answerFirst} ${answerFirst}.`);















