"use strict";

const array = createArrayOfData();
console.log(array);
console.log('without string ');
console.log(filterArray(array, 'string'));
console.log('without number ');
console.log(filterArray(array, 'number'));
console.log('without object ' );
console.log(filterArray(array, 'object'));
console.log('without bigint ');
console.log(filterArray(array, 'bigint'));
console.log('without boolean ');
console.log(filterArray(array, 'boolean'));
console.log('without undefined ');
console.log(filterArray(array, 'undefined'));
console.log('without null ');
console.log(filterArray(array, 'null'));

function createArrayOfData() {
    return [2n ** 53n, {a: 1, b: 2},{a: 3, b: 4},11, 'test','test2', 5, true,false, undefined, null];
}

function createArrayOfTypes() {
    return ['string', 'number', 'object', 'bigint', 'boolean', 'undefined', 'null'];
}

function filterArray(array, type) {
    const typeChosen = choseType(type);
    const arrayOfTypes = createArrayOfTypes();
    if (array !== undefined && array.length > 0 && arrayOfTypes.includes(typeChosen)
    ) {
        return array.filter(el => {
            if (typeChosen === 'null') {
                return el !== null;
            } else if (typeChosen === 'object') {
                if(el===null){
                    return true;
                } else{
                    return typeof el !== typeChosen;
                }
            } else {
                return typeof el !== typeChosen
            }
        })
    } else {
        console.error('wrong data');
    }
}

function choseType(type) {
    if (type + '' === 'null') {
        return 'null';
    } else {
        return type + '';
    }
}