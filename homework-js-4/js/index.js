"use strict";

const obj = createNewUser();
console.log(obj.getLogin());

function createNewUser() {
    let name;
    let lastName;
    do {
        name = prompt('Enter your name');
        lastName = prompt('Enter your last name');
    } while (validateNameAndLastName(name) || validateNameAndLastName(lastName));

    const newUser = {
        _firstName: name,
        _lastName: lastName,

        getLogin() {
            return this._firstName[0].toLowerCase() + this._lastName.toLowerCase()
        },

        set firstName(value) {
            this._firstName = value;
        },

        set lastName(value) {
            this._lastName = value;
        },

        get firstName() {
            return this._firstName;
        },

        get lastName() {
            return this._lastName;
        }
    };
    setPrivateFields(newUser);

    return newUser
}
 function setPrivateFields(obj){
     Object.defineProperties(obj, {
             '_firstName':
                 {
                     writable: false
                 },
             '_lastName':
                 {
                     writable: false
                 },
         }
     );
 }

 function validateNameAndLastName(answer){
     return answer==="" || answer===null;
}
