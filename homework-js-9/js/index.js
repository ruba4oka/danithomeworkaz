"use strict";

const tabListActions = document.querySelector('.tabs');
const tabListContent = document.querySelector('.tabs-content');

tabListActions.addEventListener("click", choseTabText);

onInit();

function choseTabText(event) {
    const eventTargetId = event.target.getAttribute('id');
    changeActiveTab(eventTargetId);
    const listOfElements = [...tabListContent.getElementsByTagName('li')]
    listOfElements.forEach(el => {
        if (!(el.getAttribute('id').startsWith(eventTargetId))) {
            el.className = 'hide-element'
        } else {
            el.className = 'show-element'
        }
    });
}

function changeActiveTab(eventTargetIdSelected){
    const listOfTabsElements = [...tabListActions.getElementsByTagName('li')]
    listOfTabsElements.forEach(el => {
        if (!(el.getAttribute('id').startsWith(eventTargetIdSelected))) {
            el.classList.remove('active');
        } else {
            el.classList.add('active');
        }
    });
}

function onInit() {
    initFilter(getInitialActiveTabId());
}

function initFilter(idInit) {
    const listOfElements = [...tabListContent.getElementsByTagName('li')]
    listOfElements.forEach(el => {
        if (!(el.getAttribute('id').startsWith(idInit))) {
            el.className = 'hide-element'
        } else {
            el.className = 'show-element'
        }
    });
}

function getInitialActiveTabId() {
    const listOfElements = [...tabListActions.getElementsByTagName('li')]
    return listOfElements.find(el => el.classList.contains('active')).id;
}
