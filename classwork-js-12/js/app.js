"use strict";

// ##Задание 1.
//
// Написать скрипт, который создаст элемент button с текстом «Войти». При клике по
// кнопке выводить alert с сообщением: «Добро пожаловать!».
//
// Callback — это функция, которая срабатывает в ответ на событие. Совсем как в
// сервисом с функцией «перезвоните мне». Мы оставляем заявку «перезвоните мне» —
// это событие. Затем специалист на перезванивает (calls back).
//
// /\*\*
// const root = document.getElementById('root');
// const btn = document.createElement('button')
// btn.textContent = 'Войти';
// root.append(btn);
//
// let numberOfCall = 0;
// const alertSomething = (event) => {
//     let text = 'Войти';
//     if (event.type === 'mouseover' && numberOfCall === 0) {
//         text = '«При клике по кнопке вы войдёте в систему.»';
//         numberOfCall += 1;
//         alert(text);
//     } else if (event.type === 'click') {
//         alert(text);
//     }
// }
//
//
// btn.addEventListener('click', alertSomething, true);
// btn.addEventListener('mouseover', alertSomething, true);

// Улучшить скрипт из предыдущего задания. При наведении на кнопку указателем мыши,
//     выводить alert с сообщением: «При клике по кнопке вы войдёте в систему.».
// Сообщение должно выводиться один раз.
//
//     Условия:
//
// -  Решить задачу грамотно.

const root = document.getElementById('root');

const elH1 = document.createElement('h1');
elH1.textContent = '«Добро пожаловать!»';
const btnChangeColor = document.createElement('button');
btnChangeColor.textContent = "«Раскрасить»";
root.append(elH1);
root.append(btnChangeColor);

btnChangeColor.addEventListener("click", changecolor, true);


function changecolor() {
    const array = elH1.textContent.split('');
    elH1.textContent='';
    array.forEach(el => {
        let span = document.createElement('span');
        span.textContent = el;
        span.style.color = getRandomColor();
        elH1.append(span);
    })


}

// Создать элемент h1 с текстом «Добро пожаловать!». Под элементом h1 добавить
// элемент button c текстом «Раскрасить». При клике по кнопке менять цвет каждой
// буквы элемента h1 на случайный.
//
// /_ Дано _/ const PHRASE = 'Добро пожаловать!';


function getRandomColor() {
    const r = Math.floor(Math.random() * 255);
    const g = Math.floor(Math.random() * 255);
    const b = Math.floor(Math.random() * 255);

    return `rgb(${r}, ${g}, ${b})`;
}




