"use strict";

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


addElementsToDOM(books, 'root');

function addElementsToDOM(array, parentElement) {
    let rootElement = document.getElementById(parentElement);
    if (rootElement === null) {
        rootElement = document.body;
    }
    try {
        if (books.length === 0) {
            throw new Error(`Масив книг пустий`);
        }
    } catch (error){
        console.error(error.message);
    }
    let fragment = createFragment(books);
    rootElement.append(fragment);
}


function createFragment(books) {
    let fragment = new DocumentFragment();
    const ulCreated = document.createElement('ul');
    let list = books.map(el => {
        try {
            checkProperty(el);
            let li = document.createElement('li');
            li.innerHTML = `<a>автор: ${el.author}, назва: ${el.name}, ціна: ${el.price}</a>`;
            return li;
        } catch (error) {
            console.error(error.message);
        }
    });
    list.forEach(li => {
        if (li !== undefined) {
            ulCreated.append(li);
        }
    });
    fragment.append(ulCreated);
    return fragment;
}

function checkProperty(object) {
    if (!object.hasOwnProperty('name')) {
        throw new Error(`The Object doesn\'t have 'name' property`);
    }
    if (!object.hasOwnProperty('price')) {
        throw new Error(`The Object doesn\'t have price property`);
    }
    if (!object.hasOwnProperty('author')) {
        throw new Error(`The Object doesn\'t have author property`);
    }
    return true;
}

