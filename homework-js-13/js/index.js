"use strict";

const changeTheme = document.getElementById('change-theme');
changeTheme.addEventListener('click', changeThemeExecuted);

choseTheme(localStorage.getItem('theme'));

function changeThemeExecuted() {
    const activeTheme = localStorage.getItem('theme');
    if (activeTheme === null) {
        localStorage.setItem('theme', 'purple');
        choseTheme('purple');
    } else if (activeTheme === 'purple') {
        localStorage.setItem('theme', 'basic');
        choseTheme('basic');
    } else if (activeTheme === 'basic') {
        localStorage.setItem('theme', 'purple');
        choseTheme('purple');
    }
}

function choseTheme(theme) {
    if (theme === 'purple') {
        changeElementsStyles('header-nav-section', 'header-nav-section-new-theme');
        changeElementsStyles('search-icon-link', 'search-icon-link-new-theme');
        changeElementsStyles('footer', 'footer-new-theme');
        changeElementsStyles('main-content', 'main-content-new-theme');
        changeElementsStyles('posts', 'posts-new-theme');
        changeElementsStyles('list-item-title', 'list-item-title-new-theme');
        changeElementsStyles('inner-grid-middle', 'inner-grid-middle-new-theme');
        changeElementsStyles('item-context', 'item-context-new-theme');
        changeElementsStyles('latest-news-list-item srb-btn', 'latest-news-list-item new-theme-sbr-btn-color');
        changeElementsStyles('latest-news-list-item arct-btn', 'latest-news-list-item new-theme-best-arct-btn-color');
        changeElementsStylesByTagName('h1', 'new-theme-title-color');
        changeElementsStylesByTagName('h2', 'new-theme-title-color');

    } else {
        changeElementsStyles('header-nav-section-new-theme', 'header-nav-section');
        changeElementsStyles('search-icon-link-new-theme', 'search-icon-link');
        changeElementsStyles('footer-new-theme', 'footer');
        changeElementsStyles('main-content-new-theme', 'main-content');
        changeElementsStyles('posts-new-theme', 'posts');
        changeElementsStyles('list-item-title-new-theme', 'list-item-title');
        changeElementsStyles('inner-grid-middle-new-theme', 'inner-grid-middle');
        changeElementsStyles('item-context-new-theme', 'item-context');
        changeElementsStyles('latest-news-list-item new-theme-sbr-btn-color', 'latest-news-list-item srb-btn');
        changeElementsStyles('latest-news-list-item new-theme-best-arct-btn-color', 'latest-news-list-item arct-btn');
        removeElementsStylesByTagName('h1', 'new-theme-title-color');
        removeElementsStylesByTagName('h2', 'new-theme-title-color');
    }

    function changeElementsStyles(className, newClassName) {
        const listOfElements = [...document.getElementsByClassName(className)];
        listOfElements.forEach(el => {
            el.className = newClassName
        });
    }

    function changeElementsStylesByTagName(tagName, newClassName) {
        const listOfElements = [...document.getElementsByTagName(tagName)];
        listOfElements.forEach(el => {
            el.classList.add(newClassName);
        });
    }

    function removeElementsStylesByTagName(tagName, className) {
        const listOfElements = [...document.getElementsByTagName(tagName)];
        listOfElements.forEach(el => {
            el.classList.remove(className);
        });
    }
}