"use strict";

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value) {
        this._name = value;
    }

    set age(value) {
        this._age = value;
    }

    set salary(value) {
        this._salary = value;
    }

    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {

    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this._lang = lang;
    }

    set lang(value) {
        this._lang = value;
    }

    get lang() {
        return this._lang;
    }

    get salary() {
        return super.salary * 3;
    }
}

const employee = new Employee('Anders Employee', 18, 1000);
console.log(employee);
console.log(employee.salary);
const programmer1 = new Programmer('Anders', 18, 1000, ['EN', 'UK', 'SP']);
console.log(programmer1);
console.log(programmer1.salary);
const programmer2= new Programmer('Pedro', 22, 600, ['SP']);
console.log(programmer2);
console.log(programmer2.salary);
const programmer3= new Programmer('Juanita', 22, 700, ['EN']);
console.log(programmer3);
console.log(programmer3.salary);

