"use strict";

const a = {x: 10};

delete a.x;
console.log(a);
// -  Задание 1.
// -
// -  Создать объект пользователя, который обладает тремя свойствами:
//     -  -  Имя;
// -  -  Фамилия;
// -  -  Профессия.
// -
// -  А также одним методом sayHi, который выводит в консоль сообщение 'Привет.'.
//    \*/
//


// const user = {
//     name: "Vasja", lastName: "Pupkin", profession: "developer", sayHello() {
//         console.log('Привет.')
//     }
// };

//
// -  Задание 2.
// -
// -  Расширить функционал объекта из предыдущего задания:
//     -  -  Метод sayHi должен вывести сообщение: «Привет. Меня зовут ИМЯ ФАМИЛИЯ.»;
// -  -  Добавить метод, который меняет значение указанного свойства объекта.
// -
// -  Продвинутая сложность:
//     -  Метод должен быть «умным» — он генерирует ошибку при совершении попытки
// -  смены значения несуществующего в объекте свойства. \*/
//

// const user = {
//     name: "Vasja", lastName: "Pupkin", profession: "developer", sayHello() {
//         console.log('Привет. Меня зовут ' + this.name + ' ' + this.lastName)
//     }, changeProperty(property, value) {
//         if (user.hasOwnProperty(property)) {
//             user[property] = value;
//         } else {
//             console.error('There are no property');
//         }
//     },
// };
//
// user.changeProperty('adsadasd','asdasdasd');

// -  Задание 3.
// -
// -  Расширить функционал объекта из предыдущего задания:
//     -  -  Добавить метод, который добавляет объекту новое свойство с указанным
// значением.
// -
// -  Продвинутая сложность:
//     -  Метод должен быть «умным» — он генерирует ошибку при создании нового свойства
// -  свойство с таким именем уже существует. \*/

//
// const user = {
//     name: "Vasja", lastName: "Pupkin", profession: "developer", sayHello() {
//         console.log('Привет. Меня зовут ' + this.name + ' ' + this.lastName)
//     }, changeProperty(property, value) {
//         if (user.hasOwnProperty(property)) {
//             user[property] = value;
//         }
//     }, addProperty(property, value) {
//         if (!user.hasOwnProperty(property)) {
//             user[property] = value;
//         } else {
//             console.error('Property has already exist');
//         }
//     }
// };


// -  Задание 4.
// -
// -  С помощью цикла for...in вывести в консоль все свойства
// -  первого уровня объекта в формате «ключ-значение».
// -
//     -  Продвинутая сложность:
//     -  Улучшить цикл так, чтобы он умел выводить свойства объекта второго уровня
// вложенности. \*/

const user = {
    name: "Vasja", lastName: "Pupkin", profession: "developer"
    , secondLevel: {secondNmae: "second", secondLastName: "secondLastName"}
};

for (const userKey in user) {
    if (user[userKey] instanceof Object) {
        const obj = Object.create(user[userKey]);
        for (const userKeySecondLevel in obj) {
            console.log( "second level data "+ userKeySecondLevel + " : " + obj[userKeySecondLevel]);
        }
    } else {
        console.log( "first level data "+userKey + " : " + user[userKey]);
    }

}