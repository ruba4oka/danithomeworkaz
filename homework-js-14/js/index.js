"use strict";

$(document).ready(function (event) {
    $('#go-home').hide();
    $('a[href*="#"]')
        .click(function (event) {
            let target = $(this.hash);
            $('html, body').animate({
                scrollTop: target.offset().top
            }, 1000);
        });

    $(window).scroll(function () {
        const windowOuterHeight = window.outerHeight;
        if ($(this).scrollTop() > windowOuterHeight) {
            $('#go-home').show();
        } else {
            $('#go-home').hide();
        }
    });

    $("a[href='#top']").click(function () {
        $("html, body").animate({scrollTop: 0}, "slow");
        return false;
    });

    $(document).on('click', '#go-home', function () {
        $("html, body").animate({scrollTop: 0}, "slow");
    });

    $(document).on('click', '#toggle', function() {
        $( "#clients" ).slideToggle( "slow");

    });

})