"use strict";

const inputElement = document.getElementById('input-for-data');
const spanElement = document.getElementById('price-result');
const btnElement = document.getElementById('close-span-btn');
const spanBtBlock = document.getElementById('span-div');
const spanError = document.getElementById('error');

inputElement.addEventListener('focus', actionOnFocus, true);
inputElement.addEventListener('blur', actionOnNoFocus, true);


function actionOnFocus() {
    createClassnameList(inputElement, 'input-field', 'text-color-black');
}

function actionOnNoFocus() {
    if(!validateNumbers()){
        console.error('wrong type of data');
        return;
    }
    if (+inputElement.value < 0) {
        openSpan(false);
        return;
    }
    if (inputElement.value.length > 0) {
        openSpan(true);
        btnElement.addEventListener('click', closeSpan, true);
    } else {
        resetValue(false);
    }
}

function closeSpan() {
    resetValue(true);
    btnElement.removeEventListener('click', closeSpan, true);
}

function openSpan(success) {
    if (success) {
        createClassnameList(spanError, 'hide-element');
        createClassnameList(spanBtBlock, 'show-element', 'margin-left-item');
        createClassnameList(inputElement, 'text-color-green');
        addContext(spanElement, inputElement.value);
    } else {
        createClassnameList(spanError, 'show-element', 'error');
        createClassnameList(spanBtBlock, 'hide-element');
    }
}

function resetValue(clearInputText) {
    createClassnameList(spanBtBlock, 'hide-element')
    createClassnameList(spanError, 'hide-element')
    if (clearInputText) {
        inputElement.value = '';
    }
}

function createClassnameList(elem, ...stylesToAdd) {
    elem.classList = [];
    stylesToAdd.forEach(el => elem.classList.add(el));
}


function addContext(el, content) {
    el.textContent = ` Текущая цена: ${content}`;
}

function validateNumbers(){
    return inputElement.value!=='' && !isNaN(+inputElement.value) && Number.parseFloat(inputElement.value).toString().length===inputElement.value.length;
}

