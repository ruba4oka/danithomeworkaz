"use strict";

// const fn = (a, b) => a + b;
// const fn = (a, b) => {return a + b};
// console.log(fn(5,7))


// Задание 2.
// -
// -  Написать функцию-сумматор всех своих параметров.
// -
// -  Функция принимает произвольное количество параметров.
// -  Однако каждый из них обязательно должен быть числом.
// -
// -  Генерировать ошибку, если:
// -  -  Если функция была вызвана менее, чем с двумя аргументами;
// -  -  Хоть один из аргументов не является допустимым числом (в ошибке указать
// его порядковый номер).
// -
//     -  Условия:
// -  -  Использовать тип функции arrow function;
// // -  -  Использова
//
// const fn = (...addAll) => {
//     let result = 0;
//     for (let i = 0; i < addAll.length; i++) {
//         if (isNaN(+addAll[i]) || typeof addAll[i] !== 'number' || !addAll[i]) {
//             continue;
//         }
//         result += addAll[i];
//     }
//     return result;
// }
//
// console.log(fn(1, 1, 1, 1));
//


// -  Задание 3.
// -
// -  Написать функцию, которая возвращает наибольшее число, из переданных ей в
// качестве аргументов при вызове.
// -
// -  Генерировать ошибку, если:
// -  -  Если функция была вызвана менее, чем с двумя аргументами;
// -  -  Хоть один из аргументов не является допустимым числом (в ошибке указать
// его порядковый номер).
// -
//     -  Условия:
// -  -  Использовать тип функции arrow function;
// -  -  Использовать объект arguments запрещено;
// -  -  Обязательно использовать объект Math. \*/


// const fn = (...addAll) => {
//     if (addAll.length < 2) {
//         console.error('Wrong');
//         return;
//     }
//     let array = [];
//     for (let i = 0; i < addAll.length; i++) {
//         if (isNaN(+addAll[i]) || typeof addAll[i] !== 'number' || !addAll[i]) {
//             console.error('Wrond data ' + i);
//             continue;
//         }
//         array.push(addAll[i]);
//     }
//     if(addAll.length===array.length){
//         return Math.max.apply(null, array);
//     }
// }
//
// console.log(fn( 1, 7));
//
//
// const getMaxNum = (...props) => {
//     if (props.length < 2) {
//         return 'wrong';
//     }
//     for (let i = 0; i < props.length; i++) {
//         if (
//             isNaN(+props[i])
//             || !props[i]
//             || typeof props[i] !== 'number'
//         ) {
//             return 'wrong ' + i;
//         }
//     }
//     return Math.max(...props);
// }
//
//
// console.log(getMaxNum(1, 243, 3343));
//
// -  Задание 4.
// -
// -  Написать функцию-логгер log, которая выводит в консоль сообщение указанное
// количество раз.
// -
// -  Функция обладает двумя параметрами:
//     -  -  Первый — строковый тип, сообщение для вывода;
// -  -  Второй — числовой тип, количество выводов сообщения.
// -
// -  Задать значения по-умолчанию для обеих параметров:
//     -  -  Для первого — «Внимание! Сообщение не указано.»;
// -  -  Для второго — 1;
// -
//     -  Если первый аргумент(сообщение) не передан - ПО УМОЛЧАНИЮ присвоить этому
// аргументу - "Empty message"
// -  Если второй аргумент(количество раз) не передан - ПО УМОЛЧАНИЮ присвоить
// этому аргументу значение 1. \*/


// let firstAnswer = prompt('Сообщение для вывода', 'Внимание! Сообщение не указано.');
// let seondAnswer = prompt('Количество выводов сообщения', '1');
//
//
// if (firstAnswer === null) {
//     firstAnswer = 'Empty message';
// }
// if (seondAnswer === null) {
//     seondAnswer = 1;
// }
//
// fn(firstAnswer, +seondAnswer);
//
// function fn(message, number) {
//     for (let i = 0; i < number; i++) {
//         console.log(message);
//     }
// }

const add = (a, b) => a + b;
const subtract = (a, b) => a - b;
const multiply = (a, b) => a * b;
const divide = (a, b) => a / b;

main(1, 2, add)

function main(a, b, c) {
    console.log(c(a, b));
}


