"use strict";

const obj = createNewUser();
console.log(obj);
console.log(obj.getAge());
console.log(obj.getPassword());

function createNewUser() {
    let name;
    let lastName;
    let birthday;
    do {
        name = prompt('Enter your name');
        lastName = prompt('Enter your last name');
        birthday = prompt('Enter your birthday, format \'dd.mm.yyyy\'');

    } while (validateNameAndLastName(name) || validateNameAndLastName(lastName) || validateDate(birthday));
    const newUser = {
        _firstName: name,
        _lastName: lastName,
        _birthday: birthday,

        getLogin() {
            return this._firstName[0].toLowerCase() + this._lastName.toLowerCase()
        },

        set firstName(value) {
            this._firstName = value;
        },

        set lastName(value) {
            this._lastName = value;
        },

        set birthday(value) {
            this._birthday = value;
        },

        get firstName() {
            return this._firstName;
        },

        get lastName() {
            return this._lastName;
        },

        get birthday() {
            return this._birthday;
        },

        getAge() {
            const dateParts = this._birthday.split(".");
            const dateParsed = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]);
            const dateNow = new Date(Date.now());
            return dateNow.getFullYear() - dateParsed.getFullYear();
        },
        getPassword() {
            return this._firstName[0].toUpperCase() + this.lastName.toLowerCase() + this._birthday.substr(6);
        }
    };

    setPrivateFields(newUser);

    return newUser
}

function setPrivateFields(obj) {
    Object.defineProperties(obj, {
            '_firstName':
                {
                    writable: false
                },
            '_lastName':
                {
                    writable: false
                },
            '_birthday':
                {
                    writable: false
                },
        }
    );
}

function validateNameAndLastName(answer) {
    return answer === "" || answer === null;
}

function validateDate(answer) {
    if (answer !== null && answer.length === 10 && answer[2] === '.' && answer[5] === '.') {
        return false;
    }
    return true;
}

