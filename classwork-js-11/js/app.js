// // "use strict";
//
// // Задание 1.
// // -
// // -  Написать скрипт, который создаст квадрат произвольного размера.
// // -
// // -  Размер квадрата в пикселях получить интерактивно посредством диалогового окна
// // prompt.
// // -
// // -  Если пользователь ввёл размер квадрата в некорректном формате —
// // -  запрашивать данные повторно до тех пор, пока данные не будут введены
// // корректно.
// // -
// // -  Все стили для квадрата задать через JavaScript посредством одной строки кода.
// // -
// // -  Тип элемента, описывающего квадрат — div.
// // -  Задать ново-созданному элементу CSS-класс .square.
// // -
// // -  Квадрат в виде стилизированного элемента div необходимо
// // -  сделать первым и единственным потомком body документа. \*/
//
// // createSquare();
//
//
// function createSquare() {
//     let size = getData();
//     while (!size || isNaN(+size)) {
//         size = getData();
//     }
//     const root = document.getElementById('root');
//     const elem = document.createElement('div');
//     elem.style = `width:${size}px; height:${size}px; border: 1px solid black`;
//     elem.className = 'square'
//     root.replaceWith(elem);
//     return elem;
// }
//
// function getData() {
//     return prompt('Enter number');
// }
//
// function getDataMax() {
//     return prompt('Enter max',);
// }
//
// // Задание 2.
// // -
// // -  Написать функцию-фабрику квадратов createSquares.
// // -
// // -  Функция обладает двумя параметром — количеством квадратов для создания.
// // -
// // -  Если пользователь ввёл количество квадратов для создания в недопустимом
// // формате —
// // -  запрашивать данные повторно до тех пор, пока данные не будут введены
// // корректно.
// // -
// // -  Максимальное количество квадратов для создания — 10.
// // -  Если пользователь решил создать более 10-ти квадратов — сообщить ему о
// // невозможности такой операции
// // -  и запрашивать данные о количестве квадратов для создания до тех пор, пока они
// // не будут введены корректно.
// // -
// // -  Размер каждого квадрата в пикселях нужно получить интерактивно посредством
// // диалогового окна prompt.
// // -
// // -  Если пользователь ввёл размер квадрата в недопустимом формате —
// // -  запрашивать данные повторно до тех пор, пока данные не будут введены
// // корректно.
// // -
// // -  Цвет каждого квадрата необходимо запросить после введения размера квадрата в
// // корректном виде.
// // -
// // -  Итого последовательность ввода данных о квадратах выглядит следующим образом:
// //     -  -  Размер квадрата n;
// // -  -  Цвет квадрата n;
// // -  -  Размер квадрата n + 1;
// // -  -  Цвет квадрата n + 1.
// // -  -  Размер квадрата n + 2;
// // -  -  Цвет квадрата n + 3;
// // -  -  и так далее...
// // -
// //     -  Если не любом этапе сбора данных о квадратах пользователь кликнул по кнопке
// //    «Отмена»,
// // -  необходимо остановить процесс создания квадратов и вывести в консоль
// // сообщение:
// //     -  «Операция прервана пользователем.».
// // -
// //     -  Все стили для каждого квадрата задать через JavaScript за раз.
// // -
// // -  Тип элемента, описывающего каждый квадрат — div.
// // -  Задать ново-созданным элементам CSS-классы: .square-1, .square-2, .square-3 и
// // так далее.
// // -
// // -  Все квадраты необходимо сделать потомками body документа. \*/
// //
// // /\*\*
//
// let size = getData();
// while (!size || isNaN(+size)) {
//     size = getData();
// }
// let maxQuantity = getDataMax();
// while (!maxQuantity || isNaN(+maxQuantity) || +maxQuantity > 10) {
//     if (+maxQuantity > 10) {
//         maxQuantity = getDataMax();
//     } else {
//         maxQuantity = getDataMax();
//     }
// }
//
// createSquares(size, maxQuantity);
//
//
// function createSquares(quantity, maxQuantity) {
//
//     for (let i = 0; i < quantity; i++) {
//         let getWithHeight = getDatWidthHeight();
//         if (getWithHeight === null) {
//             alert('Операция прервана пользователем.')
//             break;
//         }
//         while (!getWithHeight || isNaN(+getWithHeight)) {
//             getWithHeight = getDatWidthHeight();
//         }
//
//         let getColor = getDataColor();
//         if (getColor === null) {
//             alert('Операция прервана пользователем.')
//             break;
//         }
//         while (!getColor || !isNaN(+getColor)) {
//             getColor = getDataColor();
//         }
//         console.log(getWithHeight);
//         console.log(getColor);
//         let classname = 'sguare-';
//         createSquareSecondTask(getWithHeight,getColor,classname);
//     }
//
//
// }
//
// function createSquareSecondTask(size,color,classNmameToAdd) {
//     const root = document.getElementById('root');
//     const elem = document.createElement('div');
//     elem.style = `width:${size}px; height:${size}px; border: 1px solid black; background-color: ${color};`;
//     elem.className = classNmameToAdd
//     root.append(elem);
//     return elem;
//
// }


// Задание 3.
// -
// -  Написать функцию fillChessBoard, которая развернёт «шахматную» доску 8 на 8.
// -
// -  Цвет тёмных ячеек — #161619.
// -  Цвет светлых ячеек — #FFFFFF.
// -  Остальные стили CSS для доски и ячеек готовы.
// -
// -  Доску необходимо развернуть внутри элемента с классом .board.
// -
// -  Каждая ячейка доски представляет элемент div с классом .cell. \*/
//
// /_ Дано _/ const LIGHT_CELL = '#ffffff'; const DARK_CELL = '#161619'; const
//     V_CELLS = 8; const H_CELLS = 8;
//
//

fillChessBoard();

function fillChessBoard() {
    const board = document.createElement('section');
    board.classList.add('board');
    document.body.prepend(board);
    const root = document.getElementById('root');
    root.replaceWith(board);
    board.style = 'display: flex; width: 160px; flex-wrap: wrap'
    const elemBlack = document.createElement('div');
    elemBlack.style = `width:20px; height:20px; background-color: #161619`;
    const elemWhite = document.createElement('div');
    elemWhite.style = `width:20px; height:20px; background-color: #ffffff`;

    for (let i = 0; i < 64; i++) {
        board.append(i % 2 === 0 ? elemBlack.cloneNode(true) : elemWhite.cloneNode(true));
    }

    // board.append(elemWhite.cloneNode(true));
    // board.append(elemBlack.cloneNode(true));
    // board.append(elemWhite.cloneNode(true));
    // board.append(elemBlack.cloneNode(true));
    // board.append(elemWhite.cloneNode(true));
    // board.append(elemBlack.cloneNode(true));
    // board.append(elemWhite.cloneNode(true));
}


//
//
// function getData() {
//     return prompt('Enter number');
// }
//
// function getDataMax() {
//     return prompt('Enter max',);
// }
//
// function getDatWidthHeight() {
//     return prompt('Enter Width Height',);
// }
//
// function getDataColor() {
//     return prompt('Enter color',);
// }