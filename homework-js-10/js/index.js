"use strict";


const submitForm = document.getElementById('submit-form');
const submitBtn = document.querySelector('.btn');
submitBtn.addEventListener('click', submitExecuted);
submitForm.addEventListener('click', changeIcon);


function changeIcon(event) {
    if (event.target.id === 'password-data') {
        openHideInputIcon(event);
    }

    if (event.target.id === 'password-data-confirm') {
        openHideInputIcon(event)
    }

}

function openHideInputIcon(event) {
    if (event.target.classList.contains('fa-eye')) {
        changeClass(event.target, 'fa-eye', 'fa-eye-slash');
        showHideInput(event.target, 'text');
    } else {
        changeClass(event.target, 'fa-eye-slash', 'fa-eye');
        showHideInput(event.target, 'password');
    }
}

function changeClass(element, deleteClass, addClass) {
    element.classList.remove(deleteClass);
    element.classList.add(addClass);
}

function showHideInput(element, typeSelected) {
    const selectedInput = element.id === 'password-data' ? 'password-data-input' : 'password-data-confirm-input'
    document.getElementById(selectedInput).type = typeSelected;
}

function submitExecuted(event) {
    if (validatePasswords()) {
        showHideError('hide');
        alert('You are welcome');
    } else {
        showHideError('show');
    }
    event.preventDefault();
}

function validatePasswords() {
    const firstData = "" + document.getElementById('password-data-input').value;
    const secondData = "" + document.getElementById('password-data-confirm-input').value
    return (firstData !== '' && secondData !== '') && (firstData === secondData);
}

function showHideError(action) {
    const errorParagraphList = document.getElementById('error-text');
    if (action === 'show') {
        errorParagraphList.classList.replace('hide', 'show');

    } else if (action === 'hide') {
        errorParagraphList.classList.replace('show', 'hide');
    }
}