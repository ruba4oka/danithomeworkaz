"use strict";


const listOfImg = [...document.getElementsByClassName('image-to-show')];
const stopBtn = document.getElementById('stop');
const continueBtn = document.getElementById('continue');
stopBtn.addEventListener('click', stopShow)

onInit(listOfImg)

let interval = setInterval(changeImg, 3000)

function onInit(list) {
    list[0].classList.add('active');

    for (let i = 0; i < list.length; i++) {
        if (
            ![...list[i].classList].includes('active')
        ) {
            list[i].classList.add('hide-element')
        }
    }
}

function changeImg() {
    const findIndexOfActiveImg = listOfImg.findIndex(el => {
        return [...el.classList].includes('active')
    })
    listOfImg[findIndexOfActiveImg].classList.remove('active')
    listOfImg[findIndexOfActiveImg].classList.add('hide-element')
    if (findIndexOfActiveImg !== listOfImg.length - 1) {
        listOfImg[findIndexOfActiveImg + 1].classList.remove('hide-element')
        listOfImg[findIndexOfActiveImg + 1].classList.add('active')
    } else {
        listOfImg[0].classList.remove('hide-element')
        listOfImg[0].classList.add('active')
    }
}

function stopShow() {
    clearInterval(interval);
    continueBtn.addEventListener('click', continueShow);
}

function continueShow() {
    interval = setInterval(changeImg, 3000);
    continueBtn.removeEventListener('click', continueShow);
}
