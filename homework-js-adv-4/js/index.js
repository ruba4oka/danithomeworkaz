"use strict";

const root = document.getElementById('root');
const btn = document.getElementById('get-films');
btn.addEventListener("click", getFilms);

function getFilms() {
    fetch('https://ajax.test-danit.com/api/swapi/films')
        .then(response => response.json())
        .then(films => {
            films.forEach(f => {
                const film = new Film(f.episodeId, f.name, f.openingCrawl, f.characters, root);
                film.renderFilm()
                const characters = new Characters(film.characters, f.episodeId);
                characters.getCharacters();
            });
        })
        .catch(error => {
            console.error(error);
        });
}

class Film {
    constructor(episodeId, name, openingCrawl, characters, root) {
        this.episodeId = episodeId;
        this.name = name;
        this.openingCrawl = openingCrawl;
        this.characters = characters;
        this.root = root;
    }

    renderFilm() {
        const ul = document.createElement("ul");
        ul.append(this.createLi('Епізод: ', this.episodeId));
        ul.append(this.createLiWithId(this.episodeId, 'Назва: ', this.name));
        ul.append(this.createLi('Зміст: ', this.openingCrawl));
        this.root.append(ul);
    }

    createLi(name, value) {
        const li = document.createElement("li");
        li.textContent = name + ' ' + value;
        return li;
    }

    createLiWithId(id, name, value) {
        const li = document.createElement("li");
        li.textContent = name + ' ' + value;
        li.setAttribute('id', id);
        return li;
    }
}

class Characters {
    constructor(characters, episodId) {
        this.characters = characters;
        this.episodId = episodId;
    }

    getCharacters() {
        this.characters.forEach(c => {
            fetch(c)
                .then(response => response.json())
                .then(character => {
                    this.renderCharacter(character)
                })
                .catch(error => {
                    console.error(error);
                });
        })
    }

    renderCharacter(
        {
            birthYear = '', eyeColor = '', gender = '', hairColor = '',
            height = '', mass = '', name = '', skinColor = ''
        }
    ) {
        const film = document.getElementById(this.episodId);
        const ul = document.createElement("ul");
        const br = document.createElement("br");
        this.createLi('Дата народження : ', birthYear, ul);
        this.createLi('Колір очей : ', eyeColor, ul);
        this.createLi('Стать : ', gender, ul);
        this.createLi('Колір очей : ', hairColor, ul);
        this.createLi('Ріст : ', height, ul);
        this.createLi('Вага : ', mass, ul);
        this.createLi('Ім\'я : ', name, ul);
        this.createLi('Колір шкіри : ', skinColor, ul);
        film.append(ul);
        film.append(br);
    }

    createLi(name, value, ul) {
        if (value !== null && value !== '') {
            const li = document.createElement("li");
            li.textContent = name + ' ' + value;
            ul.append(li);
        }
    }
}
