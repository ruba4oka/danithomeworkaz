"use strict";

// const p = document.querySelector("p");
// const input = document.querySelector("input");

//
// Basic /\*\*
//
// -  Задание 1.
// -
// -  Создать элемент h1 с текстом «Нажмите любую клавишу.».
// -
//     -  При нажатии любой клавиши клавиатуры менять текст элемента h1 на:
//     -  «Нажатая клавиша: ИМЯ_КЛАВИШИ». \*/
//
// /\*\*

// const elem = document.createElement('h1');
// elem.textContent = 'Нажмите любую клавишу.';
// window.addEventListener('keydown', (event) => {
//     elem.textContent = `Нажатая клавиша:${event.key}`;
// })
// document.body.prepend(elem);


// При вводе текта в инпут "на лету" выводить вводимый текст в параграф ниже».


// const elem = document.createElement('h1');
// elem.textContent = 'Нажмите любую клавишу.';
// window.addEventListener('keydown', (event) => {
//     elem.textContent = `Нажатая клавиша:${event.key}`;
// })
// document.body.prepend(elem);

// const p = document.getElementById('text-from-input');
// const input = document.getElementById("my-input");
//
// input.addEventListener('keydown',()=>{
//     p.textContent = input.value;
// });
//
// console.log()


// -  Задача 1 (событие прокрутки)
//
// На странице с марджинов 234vh расположен абзац текста. Прокрутка страницы
// доступна. При прокрутке страницы, когда весь параграф появится на странице
// вывести в алерт сообщение "Теперь вам все видно!". Под параграфом расстояние до
// конца страницы не ограничено.


// const p = document.getElementById('my-paragraph');
// // const topPX = p.offsetTop;
// // const pHeight = p.offsetHeight;
// // console.log(topPX + pHeight);
//
// const myFunction = () => {
//     const topPX = p.getBoundingClientRect().top;
//     const pHeight = p.offsetHeight;
//     if ((topPX + pHeight) <= window.innerHeight) {
//         alert('hello');
//         window.removeEventListener('scroll', myFunction);
//     }
// }
//
// window.addEventListener('scroll', myFunction);


// Задача 2 (события клавиатуры)
//
// На странице расположено текстовое поле textarea. Создать функцию, которая
// ограничивает ввод данных, запрещая вводить цифры. При попытке пользователя
// ввести цыфры выводить под полем текст ошибки: "Вводить можно только буквы и
// символы!"


const ourElem = document.getElementById('textBlock');
const myFunction = (elem) => {
    const ifExist = document.querySelector('p');
    if (elem.code.startsWith('Digit')) {
        if (ifExist === null) {
            const newElem = document.createElement('p');
            newElem.textContent = 'Вводить можно только буквы и символы!';
            ourElem.after(newElem);
        }
        elem.preventDefault();
    } else {
        if (ifExist !== null) {
            document.querySelector('p').remove();
        }
    }

    if (elem.target.value.length === 30) {
        elem.preventDefault();
    }
}
ourElem.addEventListener('keydown', myFunction);






