"use strict";
// Задание 1.
// -
// -  Получить и вывести в консоль следующие элементы страницы:
//     -  -  По идентификатору (id): элемент с идентификатором list;
// -  -  По классу — элементы с классом list-item;
// -  -  По тэгу — элементы с тэгом li;

// -  -  По CSS селектору (один элемент) — третий li из всего списка;
// -  -  По CSS селектору (много элементов) — все доступные элементы li.
// -
// -  Вывести в консоль и объяснить свойства элемента:
//     -  -  innerText;
// -  -  innerHTML;
// -  -  outerHTML. \*/

// const listById = document.getElementById("list");
// console.log(listById);
// const listItemByClass = document.getElementsByClassName("list-item");
// console.log(listItemByClass);
// const liByTag = document.getElementsByTagName("li");
// console.log(liByTag);
// const elementByQueryCss = document.querySelector("li:nth-child(3)");
// console.log(elementByQueryCss);
// const elementsByQueryCss = document.querySelectorAll("li");
// console.log(elementsByQueryCss);
//
// console.log(elementByQueryCss.innerText);
// console.log(elementByQueryCss.innerHTML);
// console.log(elementByQueryCss.outerHTML);


// -  Задание 2.
// -
// -  Получить элемент с классом .remove.
// -  Удалить его из разметки.
// -
// -  Получить элемент с классом .bigger.
// -  Заменить ему CSS-класс .bigger на CSS-класс .active.
// -
// -  Условия:
// -  -  Вторую часть задания решить в двух вариантах: в одну строку и в две
// строки.
//
// /\*

// const elementByQueryCss =  document.querySelectorAll(".remove")[0];
// const listByQueryCss =  document.querySelectorAll(".list")[0];
// listByQueryCss.removeChild(elementByQueryCss);
//
// const getBiggerElement =  document.querySelectorAll(".bigger")[0];
// const newClass = getBiggerElement.className.replaceAll("bigger","active");
// getBiggerElement.className = newClass;


// Задание 3.
// -
// -  На экране указан список товаров с указанием названия и количества на складе.
// -
// -  Найти товары, которые закончились и:
//     -  -  Изменить 0 на «закончился»;
// -  -  Изменить цвет текста на красный;
// -  -  Изменить жирность текста на 600.
// -
// -  Требования:
// -  -  Цвет элемента изменить посредством модификации атрибута style.
//
// \*


const listOfElements = document.getElementsByTagName("li");
const list = Array.of(...listOfElements);
list.forEach(el => {
    if (el.innerText.split(':')[1].trim()==='0') {
        const newText = el.innerText.replace('0', 'закончился');
        el.innerText = newText;
        el.style.color='red';
        el.style.fontWeight='600';
    }
});


