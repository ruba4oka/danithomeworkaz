"use strict";

const root = document.getElementById('root');
const USERS_URL = 'https://ajax.test-danit.com/api/json/users';
const POSTS_URL = 'https://ajax.test-danit.com/api/json/posts';
const DELETE_POST_URL = 'https://ajax.test-danit.com/api/json/posts/';
const HTTP_STATUS_OK = 200;

Promise.all([
    fetch(USERS_URL),
    fetch(POSTS_URL),
]).then(response => {
    const array = [];
    response.forEach(el => {
        array.push(el.json());
    })
    return array;
}).then(parsed => {
    parsed[0].then(users => {
        const allUsers = [];
        const allPost = [];
        users.forEach(userSelected => {
            allUsers.push(new User(userSelected))
        });
        parsed[1].then(posts => {
            posts.forEach(postSelected => {
                allPost.push(new Post(postSelected))
            });
            allPost.forEach(postForCard => {
                const userForCard = allUsers.find(x => x.id === postForCard.userId);
                const card = new Card(userForCard, postForCard);
                card.renderCard();
            })
        })
    })
}).catch(error => {
    console.error(error);
});


class User {
    constructor({name = '', username = '', email = '', id = ''}) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.id = id;
    }
}

class Post {
    constructor({title = '', body = '', userId = '', id = ''}) {
        this.title = title;
        this.body = body;
        this.userId = userId;
        this.postId = id;
    }
}

class Card {
    constructor(user, post) {
        this.user = user;
        this.post = post;
    }

    renderCard() {
        const card = document.createElement("div");
        card.className = "card";
        card.setAttribute('id', this.post.postId);
        this.createCardTitle(card);
        this.createContent(card);
        this.createAction(card);
        root.append(card);
        this.cardElement = card;
    }

    createCardTitle(card) {
        const cardTitle = document.createElement("span");
        cardTitle.className = 'card__title';
        this.createAndAddSpan(cardTitle, this.user.name, 'card__title-name');
        this.createAndAddSpan(cardTitle, this.user.username, 'card__title-user-name');
        this.createAndAddSpan(cardTitle, this.user.email, 'card__title-email');
        card.append(cardTitle);
    }

    createContent(card) {
        const cardContent = document.createElement("span");
        cardContent.className = 'card__content';
        this.createAndAddSpan(cardContent, 'img', 'card__content-img');
        this.createAndAddSpan(cardContent, this.post.title, 'card__content-title');
        this.createAndAddSpan(cardContent, this.post.body, 'card__content-text');
        card.append(cardContent);
    }

    createAction(card) {
        const cardActions = document.createElement("span");
        cardActions.className = 'card__actions';
        const deleteBtn = document.createElement("button");
        deleteBtn.textContent = 'Видалити';
        deleteBtn.setAttribute('id', `btn-${this.post.postId}`);

        deleteBtn.addEventListener('click', this.sendRequestForDelete.bind(event,this));

        cardActions.append(deleteBtn);
        card.append(cardActions);
    }

    createAndAddSpan(rootEl, textContent, className) {
        const sp = document.createElement("span");
        sp.textContent = textContent;
        sp.className = className;
        rootEl.append(sp);
    }

    sendRequestForDelete(element) {
        element.cardElement.remove();

        // element.remove();

        // const el = document.getElementById(event.target.id.substr(4))
        // fetch(DELETE_POST_URL + el.id, {
        //     method: 'DELETE',
        // }).then(result => {
        //     if (result.status === HTTP_STATUS_OK) {
        //         el.remove();
        //     }
        // }).catch(error => {
        //     console.error(error);
        // })
    }
}



// -приймає форми LoginForm,VisitForm  ;
class Modal {
}

// форма для створення-поля для різних лікарів; Приймає об'єкти класів VisitDentist, VisitCardiologist, VisitCardiologist
// Якщо в випадайці вибирається - (Кардиолог, Стоматолог, Терапевт). - генериться своя форма
class VisitForm {

}

// -поля для авторизації;
class LoginForm {
    email
    password

    constructor(email, password) {
        this.email = email;
        this.password = password;
    }
}

class Visit {
// айді
    visitId;
// ФИО
    name;
// ФИО
    surname;
// цель визита
    aim;
// краткое описание визита
    description;
// выпадающее поле - срочность (обычная, приоритетная, неотложная)
    urgency;
// Open/Done
    state;
// поля для дополнительных комментариев
    additionalComments;

    constructor(visitId, name, surname, aim, description, urgency, state, additionalComments) {
        this.visitId = visitId;
        this.name = name;
        this.surname = surname;
        this.aim = aim;
        this.description = description;
        this.urgency = urgency;
        this.state = state;
        this.additionalComments = additionalComments;
    }
}

class VisitDentist extends Visit {
// дата последнего посещения
    lastVisitDate;

    constructor(visitId, name, surname, aim, description, urgency, state, additionalComments, lastVisitDate) {
        super(visitId, name, surname, aim, description, urgency, state, additionalComments);
        this.lastVisitDate = lastVisitDate;
    }
}

class VisitCardiologist extends Visit {
// обычное давление
    normalPressure;
// индекс массы тела
    bodyMassIndex;
// перенесенные заболевания сердечно-сосудистой системы
    pastIllnesses;
// возраст
    age;
    constructor(visitId, name, surname, aim, description, urgency, state, additionalComments, normalPressure, bodyMassIndex, pastIllnesses, age) {
        super(visitId, name, surname, aim, description, urgency, state, additionalComments);
        this.normalPressure = normalPressure;
        this.bodyMassIndex = bodyMassIndex;
        this.pastIllnesses = pastIllnesses;
        this.age = age;
    }
}

class VisitTherapist extends Visit {
// возраст
    age;

    constructor(visitId, name, surname, aim, description, urgency, state, additionalComments, age) {
        super(visitId, name, surname, aim, description, urgency, state, additionalComments);
        this.age = age;
    }
}




